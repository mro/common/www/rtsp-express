// @ts-check

const
  net = require('node:net'),
  prom = require('@cern/prom'),
  debug = require('debug')('rtsp:tcp');

class TCPSocket {
  /** @type {net.Socket|null} */
  #sock = null;

  /** @type {boolean} */
  connected = false;

  /** @type {Promise<any>|null} */
  #conProm = null;

  /**
   *
   * @param {string} host
   * @param {number} port
   */
  constructor(host, port) {
    this.host = host;
    this.port = port;
  }

  sock() { return this.#sock; }

  async close() {
    debug('connection closed');
    const sock = this.#sock;
    this.#sock = null;
    this.#conProm = null;
    this.connected = false;
    sock?.removeAllListeners();
    sock?.end();
    sock?.destroy();
  }

  async connect(timeout = TCPSocket.CONN_TIMEOUT) {
    if (this.connected || this.#conProm) {
      return this.#conProm;
    }

    const deferred = prom.makeDeferred();
    const sock = new net.Socket();
    /** @type {(err?: Error) => any} sockConnError */
    var errorCB = (err) => {
      debug('connection error:', err?.message ?? err);
      sock.removeAllListeners();
      sock.destroy();
      deferred.reject(new Error(err?.message ?? 'unknown error'));
    };

    sock.once('error', errorCB);
    sock.once('close', errorCB);
    sock.setTimeout(timeout, errorCB);

    sock.connect(this.port, this.host, () => {
      sock.removeListener('error', errorCB);
      sock.removeListener('close', errorCB);
      debug('connection opened');
      deferred.resolve(null);
    });
    this.#conProm = deferred.promise.then(() => {
      sock.removeAllListeners('timeout');
      sock.setTimeout(0);
      sock.setNoDelay();
      sock.setKeepAlive(true, TCPSocket.KEEPALIVE_INTERVAL);
      if (this.#sock) {
        this.close();
      }
      this.#sock = sock;
      this.#sock.once('close', (hadError) => {
        debug(`socket closed (hadError: ${hadError})`);
        sock.removeAllListeners();
        this.close();
      });
      this.#sock.once('error', this.close.bind(this));
      debug('connected', sock.address());
      this.connected = true;
    });
    return this.#conProm;
  }
}

TCPSocket.CONN_TIMEOUT = 5000;
TCPSocket.KEEPALIVE_INTERVAL = 5000;

module.exports = TCPSocket;
