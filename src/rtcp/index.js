// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2023 CERN (home.cern)
// SPDX-Created: 2023-03-31T16:03:58
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

const
  Types = require('./Types'),
  Goodbye = require('./Goodbye'),
  ReceiverReport = require('./ReceiverReport'),
  SenderReport = require('./SenderReport'),
  SourceDescription = require('./SourceDescription'),
  SourceReport = require('./SourceReport');

module.exports = {
  ...Types, ...Goodbye, ...ReceiverReport, ...SenderReport,
  ...SourceDescription, ...SourceReport
};
