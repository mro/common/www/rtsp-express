// @ts-check

const
  { BufferOut, BufferIn } = require('../utils/IOBuffer'),
  debug = require('debug')('rtsp:rtcp:packet'),
  { PacketType } = require('./Types'),
  { SourceReport } = require('./SourceReport');

/**
 * @typedef {import('../utils/VarBuffer')} VarBuffer
 */
class ReceiverReport {
  /**
   * @details identifier for the receiver
   * @type {number}
   */
  ssrc;

  /** @type {SourceReport[]} */
  reports = [];

  genSsrc() {
    this.ssrc = Math.floor(Math.random() * 0xFFFFFFFF);
  }

  /**
   * @brief serialize packet
   * @param {VarBuffer|BufferOut} buffer
   * @return {boolean}
   */
  toBuffer(buffer) {
    const io = BufferOut.wrap(buffer);

    /* Version=2 no padding */
    io.writeUInt8(0x80 | this.reports.length);
    io.writeUInt8(PacketType.ReceiverReport);
    io.writeUInt16(1 + (SourceReport.SIZE * this.reports.length), true);
    io.writeUInt32(this.ssrc, true);

    this.reports.forEach((s) => s.toBuffer(io));
    return !io.fail;
  }

  /**
   * @param {Buffer|BufferIn} buffer
   * @return {? ReceiverReport}
   */
  static fromBuffer(buffer) {
    const io = BufferIn.wrap(buffer);
    const ret = new ReceiverReport();

    let value = io.readUInt8();
    if ((value & 0xC0) !== 0x80) {
      debug('invalid ReceiverReport version: %i', (value >> 6));
      return null;
    }
    const padding = !!(value & 0x20);
    const len = value & 0x1F;

    value = io.readUInt8();
    if (value !== PacketType.ReceiverReport) {
      debug('not a ReceiverReport packet: %i', value);
      return null;
    }
    io.readUInt16(true);
    ret.ssrc = io.readUInt32(true);

    for (let i = 0; i < len; ++i) {
      const report = SourceReport.fromBuffer(io);
      if (!report) { return null; }
      ret.reports.push(report);
    }
    if (padding) {
      const len = io.readUInt8();
      if (len) { io.fwd(len - 1); }
    }
    return io.fail ? null : ret;
  }
}

module.exports = { ReceiverReport };
