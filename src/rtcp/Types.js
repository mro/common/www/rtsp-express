// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2023 CERN (home.cern)
// SPDX-Created: 2023-03-31T16:01:36
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

const
  { invert } = require('lodash'),
  { BufferIn } = require('../utils/IOBuffer'),
  debug = require('debug')('rtsp:rtcp:packet');

/**
 * @typedef {import('../utils/VarBuffer')} VarBuffer
 */

// minimum interval between RTCP packets
const RTCP_REPORT_MIN = 5000;
const RTCP_FIRST_REPORT = RTCP_REPORT_MIN / 2;
const RTCP_VERSION = 2;

// see https://www.rfc-editor.org/rfc/rfc3550#section-12.1
const PacketType = {
  ReceiverReport: 201,
  SenderReport: 200,
  SourceDescription: 202,
  Goodbye: 203,
  ApplicationDefined: 204
};
const PacketTypeName = invert(PacketType);

/**
 * @param {Buffer|BufferIn} buffer
 */
function getPacketType(buffer) {
  const io = BufferIn.wrap(buffer);
  if (io.bytesLeft() < 2) {
    debug('invalid RTCP packet type, buffer too small');
    return null;
  }
  const version = io.buffer[io.idx] >> 6;
  if (version !== RTCP_VERSION) {
    debug('invalid RTCP packet type, invalid version: %i', version);
    return null;
  }
  const type = io.buffer[io.idx + 1];
  return (PacketTypeName[type]) ? type : null;
}

module.exports = {
  PacketType, PacketTypeName,
  getPacketType, RTCP_REPORT_MIN, RTCP_FIRST_REPORT, RTCP_VERSION
};
