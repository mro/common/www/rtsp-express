// @ts-check

const
  { reduce, last, invert, size } = require('lodash'),
  { BufferOut, BufferIn } = require('../utils/IOBuffer'),
  debug = require('debug')('rtsp:rtcp:packet'),
  { PacketType } = require('./Types');

/**
 * @typedef {import('../utils/VarBuffer')} VarBuffer
 */

class SourceDescription {
  /** @type {SourceDescriptionItem[]} */
  descriptions = [];

  /**
   * @brief serialize packet
   * @param {VarBuffer|BufferOut} buffer
   * @return {boolean}
   */
  toBuffer(buffer) {
    const io = BufferOut.wrap(buffer);

    /* Version=2 no padding */
    io.writeUInt8(0x80 | size(this.descriptions));
    io.writeUInt8(PacketType.SourceDescription);
    const descSize = reduce(this.descriptions,
      (sum, d) => sum + d.getSize(), 0);
    io.writeUInt16(Math.floor(descSize + 3) / 4);

    this.descriptions.forEach((desc) => desc.toBuffer(io));
    return !io.fail;
  }

  /**
   * @param {Buffer|BufferIn} buffer
   * @return {? SourceDescription}
   */
  static fromBuffer(buffer) {
    const io = BufferIn.wrap(buffer);
    const ret = new SourceDescription();

    let value = io.readUInt8();
    if ((value & 0xC0) !== 0x80) {
      debug('invalid SourceDescription version: %i', (value >> 6));
      return null;
    }
    const padding = !!(value & 0x20);
    const len = value & 0x1F;

    value = io.readUInt8();
    if (value !== PacketType.SourceDescription) {
      debug('not a SourceDescription packet: %i', value);
      return null;
    }
    io.readUInt16(true);

    for (let i = 0; i < len; ++i) {
      const item = SourceDescriptionItem.fromBuffer(io);
      if (!item) { return null; }
      ret.descriptions.push(item);
    }
    if (padding) {
      const len = io.readUInt8();
      if (len) { io.fwd(len - 1); }
    }
    return io.fail ? null : ret;
  }
}

class SourceDescriptionItem {
  /** @type {number} */
  ssrc;

  /** @type {{ type: number, value: string }[]} */
  items = []

  getSize() {
    let ret = 4 + reduce(this.items, (sum, item) => {
      switch (item.type) {
      case SourceDescriptionItem.Type.ZERO: return sum + 1;
      default: return sum + 2 + item.value.length;
      }
    }, 0);
    if (last(this.items)?.type !== SourceDescriptionItem.Type.ZERO) {
      /* will be automatically added */
      ret += 1;
    }

    return ret;
  }


  /**
   * @brief serialize packet
   * @param {VarBuffer|BufferOut} buffer
   * @return {boolean}
   */
  toBuffer(buffer) {
    const io = BufferOut.wrap(buffer);

    io.writeUInt32(this.ssrc, true);
    this.items.forEach((item) => {
      io.writeUInt8(item.type);
      if (item.type !== SourceDescriptionItem.Type.ZERO) {
        io.writeUInt8(item.value.length);
        io.writeString(item.value, item.value.length);
      }
    });
    if (last(this.items)?.type !== SourceDescriptionItem.Type.ZERO) {
      io.writeUInt8(SourceDescriptionItem.Type.ZERO);
    }
    return !io.fail;
  }

  /**
   * @param {Buffer|BufferIn} buffer
   * @return {? SourceDescriptionItem}
   */
  static fromBuffer(buffer) {
    const io = BufferIn.wrap(buffer);
    const ret = new SourceDescriptionItem();

    ret.ssrc = io.readUInt32(true);
    let zeroFound = false;
    while (!io.fail && !zeroFound) {
      const type = io.readUInt8();
      if (type === SourceDescriptionItem.Type.ZERO) {
        zeroFound = true;
        ret.items.push({ type, value: "" });
      }
      else {
        const len = io.readUInt8();
        ret.items.push({ type, value: io.readString(len) });
      }
    }
    return io.fail ? null : ret;
  }
}

SourceDescriptionItem.Type = {
  ZERO: 0, CNAME: 1, NAME: 2, EMAIL: 3, PHONE: 4, LOC: 5, TOOL: 6, NOTE: 7,
  PRIV: 8
};
SourceDescriptionItem.TypeName = invert(SourceDescriptionItem.Type);
SourceDescription.Item = SourceDescriptionItem;

module.exports = { SourceDescription };
