// @ts-check

const
  { BufferOut, BufferIn } = require('../utils/IOBuffer');

/**
 * @typedef {import('../utils/VarBuffer')} VarBuffer
 * @typedef {import('./SenderReport').SenderReport} SenderReport
 */

class SourceReport {
  /**
   * @details Synchronization Source identifier from RTP packets
   * @type {number}
   */
  ssrc;

  /**
   * @details portion of packets lost out of 256 since last report
   * @type {number}
   */
  fractionLost = SourceReport.FRACTION_LOST_MAX;

  /**
   * @details total number of packets lost for the stream
   * @type {number}
   */
  packetLost = -1;

  /** @type {number} */
  seqNum;

  /** @type {number} */
  jitter = 0;

  /**
   * @details middle 32bits of the 65 timestamp of last SR
   * @type {number}
   */
  lastSR = 0;

  /**
   * @details delay since last SR in 1/65536 sec
   * @type {number}
   */
  delaySR = 0;

  /**
   * @brief serialize packet
   * @param {VarBuffer|BufferOut} buffer
   * @return {boolean}
   */
  toBuffer(buffer) {
    const io = BufferOut.wrap(buffer);

    io.writeUInt32(this.ssrc, true);
    io.writeUInt8(this.fractionLost);
    io.writeUInt8((this.packetLost >> 16) & 0xFF);
    io.writeUInt16(this.packetLost & 0xFFFF, true);
    io.writeUInt32(this.seqNum, true);
    io.writeUInt32(this.jitter, true);
    io.writeUInt32(this.lastSR, true);
    io.writeUInt32(this.delaySR, true);
    return !io.fail;
  }

  /**
   * @param {Buffer|BufferIn} buffer
   * @return {? SourceReport}
   */
  static fromBuffer(buffer) {
    const io = BufferIn.wrap(buffer);
    const ret = new SourceReport();

    ret.ssrc = io.readUInt32(true);
    ret.fractionLost = io.readUInt8();
    ret.packetLost = (io.readUInt8() << 16) | io.readUInt16(true);
    ret.seqNum = io.readUInt32(true);
    ret.jitter = io.readUInt32(true);
    ret.lastSR = io.readUInt32(true);
    ret.delaySR = io.readUInt32(true);
    return io.fail ? null : ret;
  }
}

SourceReport.SIZE = 6;
SourceReport.FRACTION_LOST_MAX = 255;

module.exports = { SourceReport };
