// @ts-check

const
  { PacketType } = require('./Types'),
  { BufferOut, BufferIn } = require('../utils/IOBuffer'),
  debug = require('debug')('rtsp:rtcp:packet');

/**
 * @typedef {import('../utils/VarBuffer')} VarBuffer
 */

class Goodbye {
  /** @type {number[]} */
  ssrcs = [];

  /** @type {string} */
  reason;

  /**
   * @brief serialize packet
   * @param {VarBuffer|BufferOut} buffer
   * @return {boolean}
   */
  toBuffer(buffer) {
    const io = BufferOut.wrap(buffer);

    /* Version=2 no padding */
    io.writeUInt8(0x80 | this.ssrcs.length);
    io.writeUInt8(PacketType.Goodbye);
    const reasonLen = Math.floor((this.reason.length + 3) / 4); // padded
    io.writeUInt16(this.ssrcs.length +
      reasonLen, true);

    for (let i = 0; i < this.ssrcs.length; ++i) {
      io.writeUInt32(this.ssrcs[i], true);
    }

    io.writeUInt8(this.reason.length);
    io.writeString(this.reason, reasonLen * 4);

    return !io.fail;
  }

  /**
   * @param {Buffer|BufferIn} buffer
   * @return {? Goodbye}
   */
  // eslint-disable-next-line complexity
  static fromBuffer(buffer) {
    const io = BufferIn.wrap(buffer);
    const ret = new Goodbye();

    let value = io.readUInt8();
    if ((value & 0xC0) !== 0x80) {
      debug('invalid Goodbye version: %i', (value >> 6));
      return null;
    }
    const padding = !!(value & 0x20);
    let len = value & 0x1F;

    value = io.readUInt8();
    if (value !== PacketType.Goodbye) {
      debug('not a Goodbye packet: %i', value);
      return null;
    }
    let totalLen = io.readUInt16(true) * 4;

    for (let i = 0; i < len; ++i) {
      ret.ssrcs.push(io.readUInt32(true));
      totalLen -= 4;
    }
    if (totalLen) {
      len = io.readUInt8();
      ret.reason = io.readString(len);
      totalLen -= len + 1;
      if (totalLen) { io.fwd(totalLen); }
    }

    if (padding) {
      const len = io.readUInt8();
      if (len) { io.fwd(len - 1); }
    }
    return io.fail ? null : ret;
  }
}

module.exports = { Goodbye };
