// @ts-check

const
  { BufferOut, BufferIn } = require('../utils/IOBuffer'),
  debug = require('debug')('rtsp:rtcp:packet'),
  { PacketType } = require('./Types'),
  { SourceReport } = require('./SourceReport');

/**
 * @typedef {import('../utils/VarBuffer')} VarBuffer
 */

class SenderReport {
  /**
   * @details identifier for the sender
   * @type {number}
   */
  ssrc;

  /**
   * @details timestamp seconds
   * @type {number}
   */
  ntpSec;

  /**
   * @details timestamp fractional part (secs divided by 0x100000000)
   * @type {number}
   */
  ntpFrac;

  /**
   * @details RTP relative timestamp
   * @type {number}
   */
  rtpTimestamp;

  /**
   * @details total number of packets sent
   * @type {number}
   */
  packetSent;

  /**
   * @details total number of bytes sent
   * @type {number}
   */
  bytesSent;


  /** @type {SourceReport[]} */
  reports = [];

  /**
   * @brief serialize packet
   * @param {VarBuffer|BufferOut} buffer
   * @return {boolean}
   */
  toBuffer(buffer) {
    const io = BufferOut.wrap(buffer);

    /* Version=2 no padding */
    io.writeUInt8(0x80 | this.reports.length);
    io.writeUInt8(PacketType.SenderReport);
    io.writeUInt16(6 + (this.reports.length * SourceReport.SIZE), true);
    io.writeUInt32(this.ssrc, true);

    io.writeUInt32(this.ntpSec, true);
    io.writeUInt32(this.ntpFrac, true);
    io.writeUInt32(this.rtpTimestamp, true);
    io.writeUInt32(this.packetSent, true);
    io.writeUInt32(this.bytesSent, true);

    this.reports.forEach((s) => s.toBuffer(io));
    return !io.fail;
  }

  /**
   * @param {Buffer|BufferIn} buffer
   * @return {? SenderReport}
   */
  static fromBuffer(buffer) {
    const io = BufferIn.wrap(buffer);
    const ret = new SenderReport();

    let value = io.readUInt8();
    if ((value & 0xC0) !== 0x80) {
      debug('invalid SenderReport version: %i', (value >> 6));
      return null;
    }
    const padding = !!(value & 0x20);
    const len = value & 0x1F;

    value = io.readUInt8();
    if (value !== PacketType.SenderReport) {
      debug('not a SenderReport packet: %i', value);
      return null;
    }
    io.readUInt16(true);
    ret.ssrc = io.readUInt32(true);

    ret.ntpSec = io.readUInt32(true);
    ret.ntpFrac = io.readUInt32(true);
    ret.rtpTimestamp = io.readUInt32(true);
    ret.packetSent = io.readUInt32(true);
    ret.bytesSent = io.readUInt32(true);

    for (let i = 0; i < len; ++i) {
      const report = SourceReport.fromBuffer(io);
      if (!report) { return null; }
      ret.reports.push(report);
    }
    if (padding) {
      const len = io.readUInt8();
      if (len) { io.fwd(len - 1); }
    }
    return io.fail ? null : ret;
  }
}

module.exports = { SenderReport };
