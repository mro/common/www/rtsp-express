// @ts-check

const
  {
    split, every, isEmpty, transform, indexOf, toNumber, isNil
  } = require("lodash"),
  debug = require('debug')('rtsp:sdp');

/**
 * @typedef {{ buffer: Buffer, index: number }} ParseContext
 * @typedef {(ctx: ParseContext, desc: ElementDesc) => void} ParsingFun
 * @typedef {{
 *  name: string,
 *  optional?: boolean,
 *  multiple?: boolean,
 *  parse?: ParsingFun,
 *  next?: ElementDescMap
 * }} ElementDesc
 * @typedef {{ [key: string]: ElementDesc }} ElementDescMap
 */

const AbsoluteUrlRe = /^[a-zA-Z][a-zA-Z0-9\-.]*:/;

/**
 * @param {string} value
 */
function isAbsoluteUrl(value) {
  return AbsoluteUrlRe.test(value ?? '');
}

/** @param {number} c */
function isCrNl(c) {
  return c === 10 || c === 13;
}

/**
 * @param {ParseContext} ctx
 * @returns any
 */
function valueParser(ctx) {
  const eol = ctx.buffer.indexOf('\n', ctx.index);
  let value;
  if (eol >= 0) {
    const hasCr = (ctx.buffer[eol - 1] === 13);
    value = ctx.buffer.subarray(ctx.index + 2, hasCr ? (eol - 1) : eol)
    .toString();
    ctx.index = eol + 1;
  }
  else {
    value = ctx.buffer.subarray(ctx.index + 2, ctx.buffer.length).toString();
    ctx.index = -1;
  }

  return value;
}


/** @type {ElementDescMap} */
const SessionDescription = {
  v: { name: 'protoVersion' },
  o: { name: 'origin', parse: originParser },
  s: { name: 'sessionName' },
  i: { name: 'sessionInfo', optional: true },
  u: { name: 'uri', optional: true },
  e: { name: 'email', optional: true, multiple: true },
  p: { name: 'phone', optional: true, multiple: true },
  c: { name: 'connection', optional: true },
  b: { name: 'bandwidth', optional: true, multiple: true },
  z: { name: 'timezone', optional: true, multiple: true },
  k: { name: 'key', optional: true, multiple: true },
  a: { name: 'attributes', optional: true, multiple: true },
  t: { name: 'time', optional: true, multiple: true, parse: timeParser },
  m: { name: 'media', optional: true, multiple: true, parse: mediaParser }
};

const TimeDescription = {
  r: { name: 'repeat', optional: true, multiple: true }
};

const MediaDescription = {
  i: { name: 'mediaTitle', optional: true, multiple: true },
  c: { name: 'connection', optional: true },
  b: { name: 'bandwidth', optional: true, multiple: true },
  k: { name: 'key', optional: true, multiple: true },
  a: { name: 'attributes', optional: true, multiple: true }
};

class SDPControllable {
  /** @type {string[]} */
  attributes = [];

  /** @type {{ [key: string]: string }} */
  #keyedAttributes;

  get keyedAttributes() {
    if (!this.#keyedAttributes) {
      this.#keyedAttributes = transform(this.attributes, (ret, value) => {
        const idx = indexOf(value, ':');
        if (idx >= 0) {
          ret[value.slice(0, idx)] = value.slice(idx + 1);
        }
      }, {});
    }
    return this.#keyedAttributes;
  }

  /**
   * @brief retrieve control url for the given media
   * @param {string} baseUrl
   * @details see https://www.rfc-editor.org/rfc/rfc2326#appendix-C.1.1
   */
  getControlUrl(baseUrl) {
    const control = this.keyedAttributes['control'];
    if (!control) {
      return null;
    }
    else if (control === '*') {
      return baseUrl;
    }
    else if (isAbsoluteUrl(control)) {
      return control;
    }
    else {
      return baseUrl + control;
    }
  }
}

class SDPOrigin {
  /** @type {string} */
  username;

  /** @type {string} */
  sessionId;

  /** @type {string} */
  sessionVersion;

  /** @type {string} */
  netType;

  /** @type {string} */
  addrType;

  check() {
    return this.username && this.sessionId && this.sessionVersion &&
      this.netType && this.addrType;
  }
}

class SDPTime {
  /** @type {string} */
  time;

  /** @type {string[]} */
  repeat = [];

  check() {
    return this.time;
  }
}

class SDPMedia extends SDPControllable {
  /** @type {string} */
  media;

  /** @type {number} */
  port;

  /** @type {number} */
  portCount;

  /** @type {string} */
  proto;

  /** @type {string[]} */
  fmt;

  /** @type {string|null} */
  connection = null;

  /** @type {string|null} */
  bandwidth = null;

  /** @type {string|null} */
  key = null;

  check() {
    return every([ 'media', 'port', 'portCount', 'proto' ],
      (v) => !isNil(this[v])) &&
      !isEmpty(this.fmt);
  }
}
/* @typedef {SDPMedia} SDPMedia */

class SDPDescription extends SDPControllable {
  /** @type {string} */
  protoVersion = '';

  /** @type {SDPOrigin} */
  origin;

  /** @type {string} */
  sessionName = '';

  /** @type {string} */
  sessionInfo = '';

  /** @type {string|null} */
  uri = null;

  /** @type {string[]|null} */
  email = null;

  /** @type {string[]|null} */
  phone = null;

  /** @type {string|null} */
  connection = null;

  /** @type {string[]|null} */
  bandwidth = null;

  /** @type {string|null} */
  timezone = null;

  /** @type {string|null} */
  key = null;

  /** @type {SDPTime[]} */
  time = [];

  /** @type {SDPMedia[]} */
  media = [];

  check() {
    return this.origin &&
      (this.connection || every(this.media, (m) => !!m?.connection));
  }
}

/**
 * @param {ParseContext} ctx
 * @param {ElementDescMap} desc
 * @param {object} out
 * @return {boolean}
 */
// eslint-disable-next-line complexity
function parseDesc(ctx, desc, out) {
  if (!(ctx?.index >= 0)) { return false; }
  while (isCrNl(ctx.buffer[ctx.index])) { ctx.index++; }

  const c = ctx.buffer[ctx.index] ?? 0;
  const elt = desc[String.fromCodePoint(c)];
  if (!elt) { return false; }

  const value = (elt.parse ?? valueParser)(ctx, elt);
  if (value === undefined) { return false; }

  if (elt?.multiple) {
    if (out[elt.name]) {
      out[elt.name].push(value);
    }
    else {
      out[elt.name] = [ value ];
    }
  }
  else {
    out[elt.name] = value;
  }
  return true;
}

/**
 * @param {ParseContext} ctx
 * @return {SDPOrigin|undefined}
 */
function originParser(ctx) {
  const values = split(valueParser(ctx), ' ');
  const ret = new SDPOrigin();
  ret.username = values[0];
  ret.sessionId = values[1];
  ret.sessionVersion = values[2];
  ret.netType = values[3];
  ret.addrType = values[4];
  return ret.check() ? ret : undefined;
}

/**
 * @param {ParseContext} ctx
 * @returns {SDPMedia|undefined}
 */
function mediaParser(ctx) {
  const out = new SDPMedia();
  const values = split(valueParser(ctx), ' ');
  out.media = values[0];

  const portInfo = values[1].split('/');
  out.port = toNumber(portInfo[0]);
  out.portCount = portInfo.length > 1 ? toNumber(portInfo[1]) : 1;

  out.proto = values[2];
  out.fmt = values.slice(3);

  while (parseDesc(ctx, MediaDescription, out)) {
    /* nothing */
  }
  return out.check() ? out : undefined;
}

/**
 * @param {ParseContext} ctx
 * @returns {SDPTime|undefined}
 */
function timeParser(ctx) {
  const out = new SDPTime();
  out.time = valueParser(ctx);

  while (parseDesc(ctx, TimeDescription, out)) {
    /* nothing */
  }
  return out.check() ? out : undefined;
}

/**
 * @param {ParseContext} ctx
 * @return {SDPDescription|undefined}
 */
function parseSession(ctx) {
  const out = new SDPDescription();
  while (parseDesc(ctx, SessionDescription, out)) {
    /* nothing */
  }
  if (!out.check()) {
    debug('failed to parse SDP session');
    return undefined;
  }
  debug('SDP session is valid');
  return out;
}

/**
 * @brief parse an SDP Session document
 * @param {Buffer} buffer
 * @returns {SDPDescription|null}
 */
function parse(buffer) {
  return parseSession({ buffer, index: 0 }) ?? null;
}

module.exports = { parse, SDPDescription, SDPMedia, SDPTime };
