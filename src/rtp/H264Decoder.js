// @ts-check

const { isNil } = require('lodash');
const { RTPHeader } = require('../rtp/RTPHeader');
const { BufferIn } = require('../utils/IOBuffer');
const { NALHeader, FUHeader } = require('./NALHeader');

const
  debug = require('debug')('rtsp:rtp:h264'),
  { EventEmitter } = require('node:events');

class H264Decoder extends EventEmitter {
  /** @type {Buffer[]} */
  #frag = [];

  /** @type {? number} */
  #seqNum;

  /**
   * @brief process incoming packet
   * @param {RTPHeader} rtpHeader
   * @param {Buffer} payload
   */
  // eslint-disable-next-line complexity
  decode(rtpHeader, payload) {
    if (rtpHeader.payloadType !== RTPHeader.PayloadType.H264) {
      debug('invalid RTP packet for H264: %i', rtpHeader.payloadType);
      return;
    }
    const io = new BufferIn(payload);
    const nal = NALHeader.fromBuffer(io);
    if (!nal) {
      debug('invalid NAL header');
      return;
    }
    switch (nal.type) {
    case NALHeader.Type.SingleUnit:
      this.#frag.length = 0;
      this.emit('data', payload.subarray(1));
      break;
    case NALHeader.Type.FUA:
      this.#onFragment(rtpHeader, io);
      break;
    case NALHeader.Type.FUB:
    case NALHeader.Type.MTAP16:
    case NALHeader.Type.MTAP24:
    case NALHeader.Type.StapA:
    case NALHeader.Type.StapB:
    default:
      debug('unsupported NAL type: %s', NALHeader.TypeName[nal.type]);
      break;
    }
  }

  /**
   * @param {BufferIn} io
   */
  #onFragment(rtpHeader, io) {
    const fu = FUHeader.fromBuffer(io);
    if (!fu) {
      debug('invalid FU header');
      this.#clearFrag();
      return;
    }
    else if (fu.start) {
      this.#frag.length = 0;
      this.#frag.push(io.buffer.subarray(2));
      this.#seqNum = rtpHeader.seqNum;
      return;
    }
    if (isNil(this.#seqNum)) {
      debug('dropping fragment, not started');
      this.#clearFrag();
      return;
    }
    const nextSeqNum = (this.#seqNum + 1) & 0xFFFF;
    if (rtpHeader.seqNum !== nextSeqNum) {
      debug('dropping fragment, invalid seqNum: %i !== %i',
        rtpHeader.seqNum, nextSeqNum);
      this.#clearFrag();
      return;
    }
    this.#seqNum = nextSeqNum;
    this.#frag.push(io.buffer.subarray(2));

    if (fu.end) {
      this.#frag.forEach((f) => this.emit('data', f));
      this.#clearFrag();
    }
  }

  #clearFrag() {
    this.#frag.length = 0;
    this.#seqNum = null;
  }
}

module.exports = { H264Decoder };
