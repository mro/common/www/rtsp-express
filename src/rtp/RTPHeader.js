// @ts-check

const
  { BufferOut, BufferIn } = require('../utils/IOBuffer'),
  debug = require('debug')('rtsp:rtp:packet');

/**
 * @typedef {import('../utils/VarBuffer')} VarBuffer
 */

const RTP_EXTENSION_BIT = 0x10;
const RTP_MARKER_BIT = 0x80;
const RTP_CSRC_COUNT_MASK = 0x0F;
const RTP_PAYLOAD_TYPE_MASK = 0x7F;

/** @enum {number} */
const RTPPayloadType = {
  H264: 96
};

class RTPHeader {
  /**
   * @details synchronization source identifier
   * @type {number}
   */
  ssrc;

  /**
   * @type {number}
   */
  payloadType;

  /**
   * @details stream marker (depends on profile)
   * @type {boolean}
   */
  marker;

  /**
   * @details data timestamp (sampled according to the stream rate)
   * starts at a random value
   * @type {number}
   */
  timestamp;

  /**
   * @details packet sequence number
   * @type {number}
   */
  seqNum;

  /**
   * @details contributing source list (up to 15)
   * @type {number[]}
   */
  csrc = [];

  /**
   * @details header extension
   * @type {? RTPHeaderExtension}
   */
  extension = null;

  /**
   * @brief serialize packet
   * @param {VarBuffer|BufferOut} buffer
   * @return {boolean}
   */
  toBuffer(buffer) {
    const io = BufferOut.wrap(buffer);

    /* Version=2 no padding */
    let value = 0x80 | this.csrc.length;
    if (this.extension) {
      value |= RTP_EXTENSION_BIT;
    }
    io.writeUInt8(value);

    value = this.payloadType;
    if (this.marker) {
      value |= RTP_MARKER_BIT;
    }
    io.writeUInt8(value);

    io.writeUInt16(this.seqNum, true);
    io.writeUInt32(this.timestamp, true);
    io.writeUInt32(this.ssrc, true);

    this.csrc.forEach((c) => io.writeUInt32(c, true));

    if (this.extension) {
      this.extension.toBuffer(io);
    }
    return !io.fail;
  }

  /**
   * @param {Buffer|BufferIn} buffer
   * @return {? RTPHeader}
   */
  static fromBuffer(buffer) {
    const io = BufferIn.wrap(buffer);
    const ret = new RTPHeader();

    let value = io.readUInt8();
    if ((value & 0xC0) !== 0x80) {
      debug('invalid RTPHeader version', (value >> 6));
      return null;
    }
    const hasExt = !!(value & RTP_EXTENSION_BIT);
    const csrcLen = value & RTP_CSRC_COUNT_MASK;

    value = io.readUInt8();
    ret.marker = !!(value & RTP_MARKER_BIT);
    ret.payloadType = value & RTP_PAYLOAD_TYPE_MASK;

    ret.seqNum = io.readUInt16(true);
    ret.timestamp = io.readUInt32(true);
    ret.ssrc = io.readUInt32(true);

    for (let i = 0; i < csrcLen; ++i) {
      ret.csrc.push(io.readUInt32(true));
    }

    if (hasExt) {
      ret.extension = RTPHeaderExtension.fromBuffer(io);
      if (!ret.extension) {
        debug('failed to parse RTPHeaderExtension');
        return null;
      }
    }
    return io.fail ? null : ret;
  }
}

RTPHeader.PayloadType = RTPPayloadType;

class RTPHeaderExtension {
  /**
   * @details profile defined type
   * @type {number}
   */
  type;

  /**
   * @type {? Buffer}
   */
  payload = null;

  /**
   * @brief serialize packet
   * @param {VarBuffer|BufferOut} buffer
   * @return {boolean}
   */
  toBuffer(buffer) {
    const io = BufferOut.wrap(buffer);

    io.writeUInt16(this.type, true);

    if (this.payload) {
      io.writeUInt16(this.payload.length, true);
      io.add(this.payload);
    }
    else {
      io.writeUInt16(0, true);
    }
    return !io.fail;
  }

  /**
   * @param {Buffer|BufferIn} buffer
   * @return {? RTPHeaderExtension}
   */
  static fromBuffer(buffer) {
    const io = BufferIn.wrap(buffer);
    const ret = new RTPHeaderExtension();

    ret.type = io.readUInt16(true);
    const len = io.readUInt16(true);
    ret.payload = io.read(len);
    return io.fail ? null : ret;
  }
}

module.exports = { RTPHeader, RTPHeaderExtension };
