// @ts-check

const
  { invert } = require('lodash'),
  { BufferOut, BufferIn } = require('../utils/IOBuffer'),
  debug = require('debug')('rtsp:rtp:packet');

/**
 * @typedef {import('../utils/VarBuffer')} VarBuffer
 */

/** @enum {number} */
const NALUnitType = {
  Invalid: 0,
  SingleUnit: 1,
  SingleUnitMax: 23,
  StapA: 24,
  StapB: 25,
  MTAP16: 26,
  MTAP24: 27,
  FUA: 28,
  FUB: 29
};

/**
 * @brief Network Abstraction Layer Units
 * @details as described in RFC6184 to transport H.264 streams
 */
class NALHeader {
  /** @type {number} */
  nri;

  /** @type {number} */
  rawType;

  /** @type {NALUnitType} */
  // eslint-disable-next-line complexity
  get type() {
    switch (this.rawType) {
    case NALUnitType.StapA:
    case NALUnitType.StapB:
    case NALUnitType.MTAP16:
    case NALUnitType.MTAP24:
    case NALUnitType.FUA:
    case NALUnitType.FUB:
      return /** @type {NALUnitType} */ (this.rawType);
    default:
      return (this.rawType >= NALUnitType.SingleUnit &&
        this.rawType <= NALUnitType.SingleUnitMax)
        ? NALUnitType.SingleUnit : NALUnitType.Invalid;
    }
  }

  /**
   * @brief serialize packet
   * @param {VarBuffer|BufferOut} buffer
   * @return {boolean}
   */
  toBuffer(buffer) {
    const io = BufferOut.wrap(buffer);

    /* Version=2 no padding */
    io.writeUInt8((this.nri << 5) | (this.rawType & 0x1F));
    return !io.fail;
  }

  /**
   * @param {Buffer|BufferIn} buffer
   * @return {? NALHeader}
   */
  static fromBuffer(buffer) {
    const io = BufferIn.wrap(buffer);
    const ret = new NALHeader();

    const value = io.readUInt8();
    if (value & 0x80) {
      debug('invalid NALHeader: zero-bit set');
      return null;
    }
    ret.nri = value >>> 5;
    ret.rawType = value & 0x1F;
    return io.fail ? null : ret;
  }
}

NALHeader.Type = NALUnitType;
NALHeader.TypeName = invert(NALUnitType);

class FUHeader {
  /** @type {boolean} */
  start;

  /** @type {boolean} */
  end;

  /** @type {number} */
  type;

  /**
   * @brief serialize packet
   * @param {VarBuffer|BufferOut} buffer
   * @return {boolean}
   */
  toBuffer(buffer) {
    const io = BufferOut.wrap(buffer);

    /* Version=2 no padding */
    let value = this.type & 0x1F;
    if (this.start) { value |= 0x80; }
    if (this.end) { value |= 0x40; }
    io.writeUInt8(value);
    return !io.fail;
  }

  /**
     * @param {Buffer|BufferIn} buffer
     * @return {? FUHeader}
     */
  static fromBuffer(buffer) {
    const io = BufferIn.wrap(buffer);
    const ret = new FUHeader();

    const value = io.readUInt8();
    ret.start = !!(value & 0x80);
    ret.end = !!(value & 0x40);
    ret.type = value & 0x1F;
    return io.fail ? null : ret;
  }
}

module.exports = { NALUnitType, NALHeader, FUHeader };
