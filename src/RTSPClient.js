// @ts-check

const httpHeaders = require('http-headers'),
  { split, forEach, toNumber, isString, assign } = require('lodash'),
  prom = require('@cern/prom'),
  debug = require('debug')('rtsp:client'),
  TCPSocket = require('./TCPSocket'),
  VarBuffer = require('./utils/VarBuffer'),
  SDPParser = require('./SDPParser');

/**
 * @typedef {httpHeaders.ResponseData & { data?: Buffer }} RTSPReply
 */

class RTSPError extends Error {
  /** @type {RTSPReply|null} */
  data = null;

  /**
   * @param {RTSPReply|string} message
   */
  constructor(message) {
    super(isString(message) ? message : (message?.statusMessage ?? message));
    if (!isString(message)) {
      this.data = message;
    }
  }
}

class RTSPClient {
  /** @type {number} */
  #cseq = 1;

  /** @type {TCPSocket} */
  #sock;

  /** @type {VarBuffer} */
  #buffer = new VarBuffer(1024);

  /** @type {RTSPReply|null} */
  #message = null;

  get connected() {
    return this.#sock.connected;
  }

  /**
   *
   * @param {string} host
   * @param {number} port
   */
  constructor(host, port) {
    this.#sock = new TCPSocket(host, port);
  }

  async close() {
    this.#sock.close();
  }

  async connect(timeout = TCPSocket.CONN_TIMEOUT) {
    await this.#sock.connect(timeout);
    const socket = this.#sock.sock();
    if (socket?.listenerCount('data') === 0) {
      this.#buffer.reset();
      this.#message = null;
      socket.on('data', this.#onData.bind(this));
    }
  }

  /**
   *
   * @param {string} cmd
   * @param {string} [path]
   * @param {any} [headers]
   * @param {number} [timeout]
   * @return {Promise<RTSPReply>}
   */
  async request(
    cmd,
    path = undefined, headers = undefined,
    timeout = RTSPClient.REQ_TIMEOUT) {

    await this.connect();
    const sock = this.#sock.sock();
    if (!sock) {
      throw new RTSPError('socket not opened');
    }

    const cseq = this.#cseq++;
    const request = [ `${cmd} ${path ?? this.getUrl()} RTSP/1.0`,
      `CSeq: ${cseq}` ];
    forEach(headers, (value, key) => request.push(`${key}: ${value}`));
    request.push('\r\n');

    const deferred = prom.makeDeferred();
    /** @type {(message: RTSPError | RTSPReply) => void} */
    const callback = (message) => {
      if (message instanceof RTSPError) {
        deferred.reject(message);
      }
      else if (toNumber(message?.headers?.['cseq']) === cseq) {
        deferred.resolve(message);
      }
    };

    sock.on('packet', callback);

    debug('request: %s', request[0]);
    sock?.write(request.join('\r\n'));
    return prom.timeout(deferred.promise, timeout).finally(
      () => sock.removeListener('packet', callback));
  }

  /**
   * @param {string} [path]
   * @param {{ [key: string]: string }} [headers]
   * @return {Promise<{ reply: RTSPReply, public: string[] }>}
   */
  async options(path = undefined, headers = undefined) {
    const reply = await this.request('OPTIONS', path, headers);
    const p = split(reply?.headers?.public, ',');
    debug('reply: OPTIONS: %o', p);
    return { reply, public: p };
  }

  /**
   * @param {string} [path]
   * @param {{ [key: string]: string }} [headers]
   * @return {Promise<{ reply: RTSPReply, baseUrl: string, sdp?: ReturnType<SDPParser.parse> }>}
   */
  async describe(path = undefined, headers = undefined) {
    headers = assign({ Accept: 'application/sdp' }, headers);
    const reply = await this.request('DESCRIBE', path, headers);
    const ret = {
      reply,
      baseUrl: reply?.headers?.['content-base'] ??
      reply?.headers?.['content-location'] ?? path ?? this.getUrl()
    };

    if (headers?.['Accept'] === 'application/sdp' && reply.data) {
      const sdp = SDPParser.parse(reply.data);
      debug('reply: SDP: %o', sdp?.media?.map((m) => m.media));
      ret.sdp = sdp;
    }
    return ret;
  }

  /**
   * @param {string} path
   * @param {string} session
   * @param {{ [key: string]: string }} [headers]
   * @return {Promise<{ reply: RTSPReply }>}
   */
  async play(path, session, headers = undefined) {
    headers = assign({ Session: session }, headers);
    const reply = await this.request('PLAY', path, headers);
    return { reply };
  }

  /**
   * @param {string} path
   * @param {string} session
   * @param {{ [key: string]: string }} [headers]
   * @return {Promise<{ reply: RTSPReply }>}
   */
  async pause(path, session, headers = undefined) {
    headers = assign({ Session: session }, headers);
    const reply = await this.request('PAUSE', path, headers);
    return { reply };
  }

  /**
   * @param {string} path
   * @param {string} transport
   * @param {{ [key: string]: string }} [headers]
   * @return {Promise<{ reply: RTSPReply, session: string }>}
   */
  async setup(path, transport, headers = undefined) {
    headers = assign({ Transport: transport }, headers);
    const reply = await this.request('SETUP', path, headers);

    let session = reply?.headers?.session ?? '';
    const idx = session.indexOf(';');
    session = (idx >= 0) ? session.slice(0, idx) : session;
    return { reply, session };
  }

  /**
   * @param {string} path
   * @param {string} session
   * @param {{ [key: string]: string }} [headers]
   * @return {Promise<{ reply: RTSPReply }>}
   */
  async teardown(path, session, headers = undefined) {
    headers = assign({ Session: session }, headers);
    const reply = await this.request('TEARDOWN', path, headers);
    return { reply };
  }

  getUrl() {
    return `rtsp://${this.#sock.host}:${this.#sock.port}/`;
  }

  /**
   * @param {Buffer} data
   */
  #onData(data) {
    const size = this.#buffer.length;
    this.#buffer.add(data);


    if (!this.#message) {
      const index = data.indexOf('\r\n\r\n');
      if (index >= 0) {
        const data = this.#buffer.slice(0, size + index + 4).toString();
        this.#buffer.shift(data.length);

        const message =
          /** @type {httpHeaders.ResponseData} */ (httpHeaders(data));
        if (message?.headers?.['connection'] === 'close' ||
          message?.statusCode > 299) {

          /* socket is not always usable after an error, better close it */
          debug('error message: %o', message);
          this.#sock.sock()?.emit('packet', new RTSPError(message));
          this.#sock.close();
        }
        else if (toNumber(message?.headers?.['content-length']) > 0) {
          this.#message = message;
        }
        else {
          debug('message: %s', message?.statusMessage);
          this.#sock.sock()?.emit('packet', message);
          return;
        }
      }
    }

    if (this.#message) {
      const contentLen = toNumber(this.#message?.headers['content-length']);
      if (this.#buffer.length >= contentLen) {
        const message = this.#message;
        this.#message = null;
        message.data = this.#buffer.slice(0, contentLen);
        this.#buffer.shift(contentLen);
        debug('message: %s', message?.statusMessage);
        this.#sock.sock()?.emit('packet', message);
      }
    }
  }
}

RTSPClient.CONN_TIMEOUT = 5000;
RTSPClient.REQ_TIMEOUT = 5000;
RTSPClient.KEEPALIVE_INTERVAL = 5000;

module.exports = RTSPClient;
