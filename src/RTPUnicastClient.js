/* eslint-disable max-lines */
// @ts-check

const { isNil, forEach } = require('lodash');
const VarBuffer = require('./utils/VarBuffer');

const
  prom = require('@cern/prom'),
  dgram = require('node:dgram'),
  debug = require('debug')('rtsp:rtp:unicast'),
  { EventEmitter } = require('node:events'),
  { BufferIn } = require('./utils/IOBuffer'),
  RTP = require('./rtp/RTPHeader'),
  {
    SenderReport, ReceiverReport, SourceDescription, Goodbye,
    PacketType, getPacketType, RTCP_FIRST_REPORT, RTCP_REPORT_MIN, SourceReport
  } = require('./rtcp');


/**
 * @typedef {import('./RTSPClient')} RTSPClient
 * @typedef {import('./SDPParser').SDPMedia} SDPMedia
 */

class SourceInfo {
  /** @type {SourceReport} */
  report = new SourceReport();

  /** @type {number} */
  received = 0;

  /** @type {? number} */
  lastReportSeqNum;

  /** @type {? number} */
  lastSenderReportTimestamp;

  /** @type {number} */
  #lastSeqNum = 0;

  /** @type {number} */
  get ssrc() {
    return this.report.ssrc;
  }

  /**
   * @brief prepare the report
   * @param {number} currentTime
   * @details flushes received and updates lastReportSeqNum
   */
  prepare(currentTime) {
    if (!isNil(this.lastReportSeqNum)) {
      if (this.report.packetLost < 0) {
        // first init
        this.report.packetLost = 0;
      }
      const expected = this.report.seqNum - this.lastReportSeqNum;
      const lost = expected - this.received;
      if (expected <= 0 || lost < 0) {
        debug('stream is inconsistent, resetting counters');
        this.report.fractionLost = SourceReport.FRACTION_LOST_MAX;
        this.report.seqNum = this.#lastSeqNum;
      }
      else {
        this.report.packetLost += lost;
        this.report.fractionLost = Math.floor(((lost << 8) >>> 0) / expected);

      }
    }
    if (!isNil(this.lastSenderReportTimestamp)) {
      this.report.delaySR = Math.round(
        (currentTime - this.lastSenderReportTimestamp) * 65.536);
    }
    this.received = 0;
    this.lastReportSeqNum = this.report.seqNum;
  }

  /**
   * @param {RTP.RTPHeader} pkt
   */
  onMessage(pkt) {
    let valid = true;
    if (isNil(this.report.seqNum)) {
      this.report.seqNum = pkt.seqNum;
    }
    else {
      const lastSeqNum = this.report.seqNum & 0xFFFF;
      let newESeqNum = (this.report.seqNum & 0xFFFF0000);
      if (lastSeqNum >= pkt.seqNum) { // overflow
        newESeqNum += 0x10000;
      }
      newESeqNum |= pkt.seqNum;

      let gap = Math.abs(newESeqNum - this.report.seqNum);
      if (newESeqNum < this.report.seqNum) {
        const overflowGap = 0x100000000 - gap;
        if (gap > overflowGap) { gap = overflowGap; }
        else {
          debug('dropping old packet: seqNum=%i gap=%i', newESeqNum, gap);
          valid = false;
        }
      }
      if (valid && gap > SourceInfo.MAX_PACKET_GAP) {
        debug('dropping corrupted packet (gap too big): seqNum=%i gap=%i',
          newESeqNum, gap);
        valid = false;
      }
      else {
        this.report.seqNum = newESeqNum;
      }
    }
    this.#lastSeqNum = pkt.seqNum;
    if (valid) {
      this.received += 1;
      return true;
    }
    return false;
  }

  /**
   * @param {SenderReport} report
   * @param {number} nowStamp
   */
  onSenderReport(report, nowStamp) {
    this.lastSenderReportTimestamp = nowStamp;
    this.report.lastSR = (((report.ntpSec & 0xFFFF) << 16) |
      ((report.ntpFrac >>> 16) & 0xFFFF)) >>> 0;
  }

  /**
   * @param {number} ssrc
   */
  constructor(ssrc) {
    this.report.ssrc = ssrc;
  }
}

SourceInfo.MAX_PACKET_GAP = 50;

/**
 * @emits
 * - data: RTP data received
 * - sr: SenderReport received
 * - sdes: Source Description received
 * - bye: Bye message received
 * - control: a control message was received (sr, sdes, bye)
 */
class RTPUnicastClient extends EventEmitter {
  /** @type {string} */
  controlUrl;

  /** @type {SDPMedia} */
  media;

  /** @type {string|null} */
  session = null;

  /** @type {dgram.Socket|null} */
  #srv = null;

  /** @type {dgram.Socket|null} */
  #csrv = null;

  /** @type {prom.Deferred<void>|null} */
  #bindDeferred = null;

  #receiverReport = new ReceiverReport();

  #receiverReportBuffer = new VarBuffer();

  /** @type {? NodeJS.Timeout} */
  #receiverReportTimer = null;

  /** @type {{ [ssrc: number]: SourceInfo }} */
  #sourceInfo = {};

  /** @type {? dgram.RemoteInfo} */
  #rinfo = null;

  /**
   * @param {RTSPClient} rtsp
   * @param {string} baseUrl
   * @param {SDPMedia} media
   */
  constructor(rtsp, baseUrl, media) {
    super();
    this.rtsp = rtsp;
    this.media = media;
    this.controlUrl = media?.getControlUrl(baseUrl) ?? '';
    this.#receiverReport.genSsrc();
  }

  async close(teardown = true) {
    this.#srv?.close();
    this.#srv?.removeAllListeners();
    this.#srv = null;

    this.#csrv?.close();
    this.#csrv?.removeAllListeners();
    this.#csrv = null;
    this.#bindDeferred = null;

    if (!isNil(this.#receiverReportTimer)) {
      clearInterval(this.#receiverReportTimer);
      this.#receiverReportTimer = null;
    }

    if (teardown && this.session && this.rtsp?.connected) {
      await this.rtsp.teardown(this.controlUrl, this.session).catch((err) => {
        debug('error sending teardown: %o', err);
      });
      this.session = null;
    }
    debug('socket closed');
  }

  /**
   * @param {{ [key: string]: string }} [headers]
   * @return {ReturnType<RTSPClient['setup']>}
   */
  async setup(headers = undefined) {
    if (this.session) {
      throw new Error('session already opened');
    }
    const reply = await this.rtsp.setup(this.controlUrl,
      await this.#getTransport(), headers);
    this.session = reply.session;
    debug('stream setup');
    return reply;
  }

  /**
   * @param {{ [key: string]: string }} [headers]
   * @return {ReturnType<RTSPClient['play']>}
   */
  async play(headers = undefined) {
    if (!this.session) {
      throw new Error('session not opened, please call setup');
    }
    const ret = await this.rtsp.play(this.controlUrl, this.session,
      headers);
    debug('playing');
    return ret;
  }

  /**
   * @param {{ [key: string]: string }} [headers]
   * @return {ReturnType<RTSPClient['pause']>}
   */
  async pause(headers = undefined) {
    if (!this.session) {
      throw new Error('session not opened, please call setup');
    }
    const ret = await this.rtsp.pause(this.controlUrl, this.session,
      headers);
    debug('paused');
    return ret;
  }

  /**
   *
   * @param {number} port
   * @returns {Promise<dgram.Socket>}
   */
  async #makeSocket(port) {
    const deferred = prom.makeDeferred();
    const sock = dgram.createSocket('udp4');

    const errHandler = (err) => {
      sock.close();
      deferred.reject(err);
    };
    sock.once('error', errHandler);
    sock.bind(port, () => {
      sock.removeListener('error', errHandler);
      deferred.resolve(sock);
    });
    return deferred.promise;
  }

  async #bind() {
    if (this.#bindDeferred) {
      return this.#bindDeferred.promise;
    }
    this.#bindDeferred = prom.makeDeferred();

    try {

      if (this.media.port) {
        console.log(this.media.port);
        const port = this.media.port & ~1; // next event port
        this.#srv = await this.#makeSocket(port);
        this.#csrv = await this.#makeSocket(port + 1);
      }
      else {
        while (!this.#srv || !this.#csrv) {
          const port = Math.floor((Math.random() * 64535) + 1000) & ~1;
          this.#srv = await this.#makeSocket(port).catch(() => null);
          this.#csrv = await this.#makeSocket(port + 1).catch(() => null);
          if (!this.#srv) { this.#csrv?.close(); }
          if (!this.#csrv) { this.#srv?.close(); }
        }
      }
      this.#srv.on('message', this.#onMessage.bind(this));
      this.#csrv.on('message', this.#onControlMessage.bind(this));
      debug('listening on %i-%i', this.#srv.address().port,
        this.#csrv.address().port);
      this.#bindDeferred.resolve();
    }
    catch (err) {
      this.#bindDeferred.reject(err);
    }
    return null;
  }

  async #getTransport() {
    await this.#bind();
    const port = this.#srv?.address().port;
    const cport = this.#csrv?.address().port;
    // eslint-disable-next-line max-len
    return `${this.media.proto};unicast;client_port=${port}-${cport}`;
  }

  /**
   * @param {Buffer} message
   */
  #onMessage(message) {
    const io = BufferIn.wrap(message);
    const header = RTP.RTPHeader.fromBuffer(io);
    if (!header) {
      debug('invalid RTP packet');
      return;
    }
    if (this.#getSourceInfo(header.ssrc).onMessage(header)) {
      this.emit('data', header, message.subarray(io.idx));
    }
  }

  /**
   * @param {Buffer} message
   * @param {dgram.RemoteInfo} rinfo
   */
  // eslint-disable-next-line complexity
  #onControlMessage(message, rinfo) {
    const io = BufferIn.wrap(message);

    while (!io.fail && io.bytesLeft()) {
      let header;
      const type = getPacketType(io);

      switch (type) {
      case PacketType.SenderReport:
        this.#rinfo = rinfo;
        header = SenderReport.fromBuffer(io);
        if (header) { this.#onSenderReport(header); }
        else { debug('invalid SenderReport packet'); }
        break;
      case PacketType.SourceDescription:
        header = SourceDescription.fromBuffer(io);
        if (header) { this.#onSourceDescription(header); }
        else { debug('invalid SourceDescription packet'); }
        break;
      case PacketType.Goodbye:
        header = Goodbye.fromBuffer(io);
        if (header) { this.#onGoodbye(header); }
        else { debug('invalid Goodbye packet'); }
        break;
      default:
        debug('unknown RTCP packet type %o', message.subarray(io.idx));
        io.fail = true;
        break;
      }
      if (header) {
        this.emit('control', header);
      }
    }
  }

  /**
   * @param {SenderReport} report
   */
  #onSenderReport(report) {
    const currentTime = Date.now();

    forEach(this.#sourceInfo,
      (srcInfo) => srcInfo.onSenderReport(report, currentTime));

    this.emit('senderReport', report);

    if (!this.#receiverReportTimer) {
      this.#sendReceiverReport();
    }
  }

  /**
   * @param {SourceDescription} desc
   */
  #onSourceDescription(desc) {
    this.emit('sourceDescription', desc);
  }

  /**
   * @param {Goodbye} pkt
   */
  #onGoodbye(pkt) {
    debug('Goodbye received, reason: %s', pkt.reason);
    this.emit('goodbye', pkt);
    this.close(false);
  }

  /**
   * @param {number} ssrc
   * @return {SourceInfo}
   */
  #getSourceInfo(ssrc) {
    let srcInfo = this.#sourceInfo[ssrc];
    if (!srcInfo) {
      debug('creating SourceInfo: %i', ssrc);
      srcInfo = this.#sourceInfo[ssrc] = new SourceInfo(ssrc);
      this.#receiverReport.reports.push(srcInfo.report);
    }
    return srcInfo;
  }

  #sendReceiverReport() {
    if (!this.#csrv) {
      debug('not sending report, socket closed');
    }
    else if (!this.#rinfo) {
      debug('not sending report, no remote info');
    }
    else {
      const currentTime = Date.now();
      forEach(this.#sourceInfo, (info) => info.prepare(currentTime));

      this.#receiverReport.toBuffer(this.#receiverReportBuffer);
      this.#csrv.send(this.#receiverReportBuffer.buffer(), this.#rinfo.port,
        this.#rinfo.address);

      if (!this.#receiverReportTimer) {
        this.#receiverReportTimer = setInterval(
          this.#sendReceiverReport.bind(this), RTCP_REPORT_MIN);
      }
    }
  }
}

module.exports = RTPUnicastClient;
