// @ts-check

const { find } = require('lodash');
const { RTSPClient, RTPUnicastClient, RTCP } = require('./src');
const { H264Decoder } = require('./src/rtp/H264Decoder');
const debug = require('debug')('test');

(async function foo() {
  const cli = new RTSPClient('127.0.0.1', 8554);
  await cli.connect();
  const options = await cli.options();
  const desc = await cli.describe();

  const video = find(desc?.sdp?.media, { media: 'video', proto: 'RTP/AVP' });
  const control = video?.getControlUrl(desc.baseUrl);
  console.log('Control Url for video: %s', control);

  const rtpCli = new RTPUnicastClient(cli, desc.baseUrl, video);
  const decoder = new H264Decoder();
  rtpCli.on('data', decoder.decode.bind(decoder));
  // rtpCli.on('data',
  //   (header, payload) => debug('data', header, payload));

  rtpCli.on('senderReport',
    (header) => debug('%O', header));

  rtpCli.on('sourceDescription', (sdes) => {
    sdes.descriptions.forEach((desc) => {
      debug('SourceDescription desc: ssrc=%i', desc.ssrc);
      desc.items.forEach((item) => {
        debug('\titem: %s=%s',
          RTCP.SourceDescription.Item.TypeName[item.type],
          item.value);
      });
    });
  });
  rtpCli.on('goodbye', (pkt) => debug('Goodbye: %O', pkt));
  await rtpCli.setup();
  await rtpCli.play();

  // await rtpCli.close();
  // cli.close();
}()).catch((err) => console.log(err));
