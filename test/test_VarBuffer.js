// @ts-check

const
  { expect } = require('chai'),
  { describe, it } = require('mocha'),
  VarBuffer = require('../src/utils/VarBuffer');


describe('VarBuffer', function() {

  it('can be enlarged on demand', function() {
    var buffer = new VarBuffer(10);
    expect(buffer.reserved()).to.be.equal(10);
    expect(buffer.length).to.be.equal(0);

    const data = Buffer.from("1", "utf8");
    for (let i = 0; i < 15; ++i) {
      buffer.add(data);
    }
    expect(buffer.length).to.be.equal(15);

    buffer.resize(30);
    expect(buffer.length).to.equal(30);
  });

  it('can be sliced like a Buffer', function() {
    var buffer = new VarBuffer(Buffer.from('0123456789'));
    expect(buffer.length).to.be.equal(10);

    expect(buffer.slice(3, 7)).to.be.deep.equal(Buffer.from('3456'));
  });

  it('can remove first n bytes', function() {
    var buffer = new VarBuffer(Buffer.from('0123456789'));
    expect(buffer.length).to.be.equal(10);

    buffer.shift(4);
    expect(buffer.buffer()).to.be.deep.equal(Buffer.from('456789'));
    expect(buffer.length).to.be.equal(6);
    expect(buffer.reserved()).to.be.equal(10);

    // negative inputs have no effect
    buffer.shift(-3);
    expect(buffer.buffer()).to.be.deep.equal(Buffer.from('456789'));
    expect(buffer.length).to.be.equal(6);
    expect(buffer.reserved()).to.be.equal(10);

    // inputs greater than buffer length imply reset
    buffer.shift(buffer.length + 1);
    expect(buffer.buffer()).to.be.deep.equal(Buffer.from(''));
    expect(buffer.length).to.be.equal(0);
    expect(buffer.reserved()).to.be.equal(10);
  });

  it('can be partially copied at a specific offset', function() {
    var bufSource = new VarBuffer(Buffer.from('0123456789'));
    var varBufTarget = new VarBuffer(Buffer.alloc(10, '-'));
    var bufTarget = Buffer.alloc(10, '.');

    bufSource.copy(varBufTarget, 3, 3, 8);
    expect(varBufTarget.buffer()).to.be.deep.equal(Buffer.from('---34567--'));

    bufSource.copy(bufTarget, 2, 0);
    expect(bufTarget).to.be.deep.equal(Buffer.from('..01234567'));
  });
});
