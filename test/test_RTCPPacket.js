// @ts-check

const
  { expect } = require('chai'),
  { describe, it } = require('mocha'),
  {
    SenderReport, ReceiverReport, SourceReport,
    SourceDescription, Goodbye, PacketType, getPacketType
  } = require('../src/rtcp'),
  VarBuffer = require('../src/utils/VarBuffer');


describe('RTCP', function() {
  it('can (de)serialize SenderReport', function() {
    const report = new SenderReport();
    report.ssrc = 1234;
    report.ntpSec = 1111;
    report.ntpFrac = 2222;
    report.rtpTimestamp = 11112222;
    report.packetSent = 4;
    report.bytesSent = 42;

    const sr = new SourceReport();
    sr.ssrc = 12345;
    sr.fractionLost = 128;
    sr.packetLost = 1123;
    sr.seqNum = 546;
    sr.jitter = 999;
    sr.lastSR = 12456;
    sr.delaySR = 890;
    report.reports.push(sr);

    const buf = new VarBuffer();
    expect(report.toBuffer(buf)).to.equal(true);
    const parsed = SenderReport.fromBuffer(buf.buffer());
    expect(parsed).to.deep.equal(report);
  });

  it('can (de)serialize ReceiverReport', function() {
    const report = new ReceiverReport();
    report.ssrc = 1234;

    const sr = new SourceReport();
    sr.ssrc = 12345;
    sr.fractionLost = 128;
    sr.packetLost = 1123;
    sr.seqNum = 546;
    sr.jitter = 999;
    sr.lastSR = 12456;
    sr.delaySR = 890;
    report.reports.push(sr);

    const buf = new VarBuffer();
    expect(report.toBuffer(buf)).to.equal(true);
    const parsed = ReceiverReport.fromBuffer(buf.buffer());
    expect(parsed).to.deep.equal(report);
  });

  it('can (de)serialize SourceDescription', function() {
    const pkt = new SourceDescription();

    let item = new SourceDescription.Item();
    item.ssrc = 1234;
    item.items = [
      { type: SourceDescription.Item.Type.CNAME, value: "test" },
      { type: SourceDescription.Item.Type.ZERO, value: "" }
    ];
    expect(item.getSize()).to.equal(4 + 6 + 1);
    pkt.descriptions.push(item);

    item = new SourceDescription.Item();
    item.ssrc = 567;
    item.items = [
      { type: SourceDescription.Item.Type.TOOL, value: "test2" }
    ];
    expect(item.getSize()).to.equal(4 + 7 + 1);
    // better for deep comparison below
    item.items.push({ type: SourceDescription.Item.Type.ZERO, value: "" });
    pkt.descriptions.push(item);

    const buf = new VarBuffer();
    expect(pkt.toBuffer(buf)).to.equal(true);
    const parsed = SourceDescription.fromBuffer(buf.buffer());
    expect(parsed).to.deep.equal(pkt);
  });

  it('can (de)serialize Goodbye', function() {
    const pkt = new Goodbye();
    pkt.ssrcs = [ 1234, 5678 ];
    pkt.reason = 'end of stream ?';

    const buf = new VarBuffer();
    expect(pkt.toBuffer(buf)).to.equal(true);
    expect(getPacketType(buf.buffer())).to.equal(PacketType.Goodbye);
    const parsed = Goodbye.fromBuffer(buf.buffer());
    expect(parsed).to.deep.equal(pkt);

  });
});
