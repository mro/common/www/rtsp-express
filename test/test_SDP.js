// @ts-check

const
  { expect } = require('chai'),
  { describe, it } = require('mocha'),
  SDP = require('../src/SDPParser');

describe('SDP', function() {
  it('can parse SDP messages', function() {
    const doc = SDP.parse(Buffer.from(`
o=- 16702395912308351890 16702395912308351890 IN IP4 balam\r
s=Unnamed
i=N/A
c=IN IP4 0.0.0.0
t=0 0
a=tool:vlc 3.0.18
a=recvonly
a=control:*
m=video 0 RTP/AVP 96
b=RR:0
a=rtpmap:96 H264/90000
a=fmtp:96 packetization-mod...
a=control:rtsp://127.0.0.1:8554/trackID=0
m=audio 0 RTP/AVP 14
    `));
    expect(doc).to.deep.contains({
      sessionName: 'Unnamed',
      sessionInfo: 'N/A',
      origin: {
        username: '-', netType: 'IN', addrType: 'IP4',
        sessionId: '16702395912308351890',
        sessionVersion: '16702395912308351890'
      },
      attributes: [ 'tool:vlc 3.0.18', 'recvonly', 'control:*' ],
      time: [ { time: '0 0', repeat: [] }  ]
    });

    expect(doc?.media).to.have.length(2);
    expect(doc?.media?.[0]).deep.contains({
      media: 'video',
      fmt: [ '96' ],
      proto: 'RTP/AVP',
      attributes: [ 'rtpmap:96 H264/90000', 'fmtp:96 packetization-mod...',
        'control:rtsp://127.0.0.1:8554/trackID=0' ]
    });
    expect(doc?.media?.[0]?.getControlUrl('xx'))
    .to.equal('rtsp://127.0.0.1:8554/trackID=0');

    expect(doc?.attributes).to.have.length(3);
    expect(doc?.getControlUrl('test://')).to.equal('test://');
  });

  it('fails to parse invalid documents', function() {
    /* no connection string anywhere */
    expect(SDP.parse(Buffer.from(`
o=- 1234 1324 IN IP4 balam\r
s=Unnamed
i=N/A
m=video 0 RTP/AVP 96
m=audio 0 RTP/AVP 14
    `))).to.be.equal(null);
  });
});
