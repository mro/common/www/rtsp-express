'use strict';

const
  process = require('process'),
  { before } = require('mocha'),
  chai = require('chai'),
  dirtyChai = require('dirty-chai');

chai.use(dirtyChai);

before(function() {
  /* do not accept unhandledRejection */
  process.on('unhandledRejection', function(reason) {
    throw reason;
  });
});
