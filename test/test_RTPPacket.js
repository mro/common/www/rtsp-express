// @ts-check

const
  { expect } = require('chai'),
  { describe, it } = require('mocha'),
  {
    RTPHeader, RTPHeaderExtension
  } = require('../src/rtp/RTPHeader'),
  VarBuffer = require('../src/utils/VarBuffer');


describe('RTP', function() {
  it('can (de)serialize RTPHeader', function() {
    const pkt = new RTPHeader();
    pkt.ssrc = 1234;
    pkt.payloadType = 96;
    pkt.marker = true;
    pkt.timestamp = 5678;
    pkt.seqNum = 4242;

    pkt.csrc = [ 9999, 8888 ];

    const ext = new RTPHeaderExtension();
    ext.type = 666;
    ext.payload = Buffer.from("123456");

    const buf = new VarBuffer();
    expect(pkt.toBuffer(buf)).to.equal(true);
    const parsed = RTPHeader.fromBuffer(buf.buffer());
    expect(parsed).to.deep.equal(pkt);
  });
});
