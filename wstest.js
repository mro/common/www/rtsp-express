// @ts-check

const { find } = require('lodash');
const { RTSPClient, RTPUnicastClient, RTCP } = require('./src');
const { H264Decoder } = require('./src/rtp/H264Decoder');
const debug = require('debug')('test');
const expres = require('express');
const ws = require('express-ws');

async function startStream(ws) {
  const cli = new RTSPClient('127.0.0.1', 8554);
  await cli.connect();
  const options = await cli.options();
  const desc = await cli.describe();

  const video = find(desc?.sdp?.media, { media: 'video', proto: 'RTP/AVP' });
  const control = video?.getControlUrl(desc.baseUrl);
  console.log('Control Url for video: %s', control);

  const rtpCli = new RTPUnicastClient(cli, desc.baseUrl, video);
  const decoder = new H264Decoder();
  decoder.on('data', (data) => ws.write(data))
  rtpCli.on('data', decoder.decode.bind(decoder));
  // rtpCli.on('data',
  //   (header, payload) => debug('data', header, payload));

  rtpCli.on('senderReport',
    (header) => debug('%O', header));

  rtpCli.on('sourceDescription', (sdes) => {
    sdes.descriptions.forEach((desc) => {
      debug('SourceDescription desc: ssrc=%i', desc.ssrc);
      desc.items.forEach((item) => {
        debug('\titem: %s=%s',
          RTCP.SourceDescription.Item.TypeName[item.type],
          item.value);
      });
    });
  });
  rtpCli.on('goodbye', (pkt) => debug('Goodbye: %O', pkt));
  await rtpCli.setup();
  await rtpCli.play();

  // await rtpCli.close();
  // cli.close();
};

var express = require('express');


var app = express();
ws(app);

app.get('/', function(req, res) {
  res.setHeader('content-type', 'video/mp4')
  startStream(res);
});
// app.ws('/', function(ws, req) {
//   startStream(ws);
//   // ws.on('message', function(msg) {
//   //   console.log(msg);
//   // });
//   // console.log('socket', req.testing);
// });

app.listen(3000);
