

// @ts-check
const child_process = require('child_process');
const debug = require('debug')('test');


var express = require('express');


var app = express();

app.get('/', function(req, res) {
  res.writeHead(200, {
    'Access-Control-Allow-Origin': '*',
    'Connection': 'Keep-Alive',
    'Content-Type': 'video/mp4'
  });
  const ffmpeg = child_process.spawn("ffmpeg", [
    // "-loglevel", "quiet",
    // "-probesize", "2147483647",
    // "-analyzeduration", "2147483647",
    "-i", 'rtsp://127.0.0.1:8554/',
    "-vcodec", "copy",
    "-f", "mp4",
    "-movflags", "frag_keyframe+empty_moov+faststart",
    "-frag_duration", "500",
    "pipe:1"
  ], { stdio: [ 'ignore', 'pipe', 'pipe' ] });
  ffmpeg.stdout.pipe(res);
  ffmpeg.stderr.setEncoding('utf8');
  ffmpeg.stderr.on('data', (data) => {
    debug('error from ffmpeg: %s', data);
  });
  ffmpeg.on('close', (code) => {
    debug('stream finished', code);
    res.sendStatus(code ? 500 : 200);
  });
});
// app.ws('/', function(ws, req) {
//   startStream(ws);
//   // ws.on('message', function(msg) {
//   //   console.log(msg);
//   // });
//   // console.log('socket', req.testing);
// });

app.listen(3000);
