# RTSP/RTP/H.264 client for Node.js

This project is a simple RTSP client that aims to decypher RTSP packets and forward it as plain H.264 to web-browsers.

## Tests

Using VLC:

```bash
# Starting a "loop" stream
cvlc -L VR_MOVIE04.mpeg --sout '#transcode{vcodec=h264,acodec=mpga,ab=128,channels=2,samplerate=44100,scodec=none}:rtp{sdp=rtsp://:8554/}' --sout-keep

# Playing the stream (use wireshark to analyse)
cvlc   rtsp://127.0.0.1:8554/
```

## Documents

- [Real Time Streaming Protocol (RTSP)](https://www.rfc-editor.org/rfc/rfc2326)
- [RTP: A Transport Protocol for Real-Time Applications](https://www.rfc-editor.org/rfc/rfc3550)
- [RTP Payload Format for H.264 Video](https://datatracker.ietf.org/doc/html/rfc6184)
- [SDP: Session Description Protocol](https://www.rfc-editor.org/rfc/rfc2327)
- [H.264](https://www.itu.int/rec/T-REC-H.264-202108-I)

## MP4

https://developer.apple.com/library/archive/documentation/QuickTime/QTFF/QTFFChap2/qtff2.html
https://www.iso.org/obp/ui/#iso:std:iso-iec:14496:-14:ed-1:v1:en
https://github.com/kevinGodell/ffmpeg-streamer
https://github.com/samsha1971/rtsp2fmp4
https://github.com/kevinGodell/node-red-mp4frag
https://stackoverflow.com/questions/47815475/spawn-ffmpeg-in-nodejs-and-pipe-to-expresss-response
